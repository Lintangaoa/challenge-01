const { mainModule, rawListeners } = require('process');
const readline = require('readline');

const interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function input(question) {
    return new Promise(resolve => {
        interface.question(question, data => {
            
            return resolve(data);
        });
    });
}

function akarKuadrat(data1) {
    return Math.sqrt(data1)
}

async function aritmatika() {
    let pilih = await input("Silahkan pilih operator(+ , - , * , / ) : ");

    let data1 = await input("Masukan angka1 : ")
    let data2 = await input("Masukan angka2 : ")
    let hasil

    if (pilih == '+') {
        hasil = +data1 + +data2

    } else if (pilih == '-') {
        hasil = +data1 - +data2

    } else if (pilih == '*') {
        hasil = +data1 * +data2

    } else if (pilih == '/') {
        hasil = +data1 / +data2
    } else {
        console.log("Option not yet")
    }

    console.log(`${data1} ${pilih} ${data2} = ${hasil}`)
    interface.close()
}

async function luasvolume() {
    console.log("1. Luas Persegi")
    console.log("2. Volume Kubus")
    console.log("3. Volume Tabung")

    let pilih = await input("Masukan pilihan : ");

    if(pilih == 1) {
        let sisi = await input("Masukan panjang sisi : ");
        let hasil = +sisi * +sisi

        console.log(`Luas persegi dengan panjang sisi ${sisi} = ${hasil}`)
        interface.close()
    } else if(pilih == 2) {
        let rusuk = await input("Masukan panjang rusuk : ");
        let hasil = +rusuk * +rusuk * +rusuk

        console.log(`Volume kubus dengan panjang rusuk ${rusuk} = ${hasil}`)
        interface.close()
    } else if(pilih == 3) {
        let r = await input("Masukan jari-jari alas : ");
        let tinggi = await input("Masukan tinggi tabung : ");
        let hasil = 22/7 * r * tinggi

        console.log(`Volume tabung = ${hasil} `)
        interface.close()
    }
}

async function main() {
    console.log("1. Aritmatika")
    console.log("2. Akar Kuadrat")
    console.log("3. Luas & Volume")

    let pilih = await input("Masukan pilihan : ");

    if (pilih == 1) {
        aritmatika()
        
    } else if (pilih == 2) {
        let data1 = await input("Masukan angka : ")
        
        console.log(`Akar dari ${data1} = `+ akarKuadrat(data1))
        interface.close()
    } else if (pilih == 3) {
        luasvolume()
        
    } else {
        console.log("Tidak ada pilihan")
        
    }
}

main()

